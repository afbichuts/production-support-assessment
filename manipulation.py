#!/usr/bin/python
import sys #import system functions
import re
import os
# Open the log file

hostfile=open("hosts.real", "r")
IPAdresses=open("IPAdresses.txt", "w")
TMP = open("TMP.txt", "w")

for line in hostfile:

    sep = '#'
    rest = line.split(sep)[0]
    newline = rest.split()
    TMP.write(" ".join(newline)+"\n")

    try:
        print(newline[-1])
        IPAdresses.write(newline[0]+"\n")

    except:
        pass

hostfile.close()
IPAdresses.close()
TMP.close()

os.remove("hosts.real")
os.rename("TMP.txt","hosts.real")
