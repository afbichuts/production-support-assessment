DECLARE @datestring VARCHAR(12) 

SET @datestring = '201202251436'

SELECT ticker,datepart(YEAR,(CONVERT(DATETIME, 
STUFF(STUFF([date],9,0,' '),12,0,':')))) AS "YEAR",datepart(MONTH,(CONVERT(DATETIME, 
STUFF(STUFF([date],9,0,' '),12,0,':')))) as "MONTH",datepart(HOUR,(CONVERT(DATETIME, 
STUFF(STUFF([date],9,0,' '),12,0,':')))) as "DAY",
SUM(vol*[close])/SUM(vol) as VOLUME_WEIGHTED_PRICE
FROM [Apple Trades]

GROUP BY ticker,datepart(YEAR,(CONVERT(DATETIME, 
STUFF(STUFF([date],9,0,' '),12,0,':')))),datepart(MONTH,(CONVERT(DATETIME, 
STUFF(STUFF([date],9,0,' '),12,0,':')))),datepart(HOUR,(CONVERT(DATETIME, 
STUFF(STUFF([date],9,0,' '),12,0,':'))))
